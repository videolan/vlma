/*
 * Copyright (C) 2008 the VideoLAN team
 *
 * This file is part of VLMa.
 *
 * VLMa is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * VLMa is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VLMa. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.videolan.vlma.model;

import java.util.HashMap;
import java.util.Map;

/**
 * Family of an adapter. Two adapters of the same family can stream the same
 * medias.
 *
 * @author Adrien Grand
 */
public abstract class AdapterFamily {

    public static class DTT extends AdapterFamily {

        public static final AdapterFamily instance = new DTT();

        private DTT() {}

        public static AdapterFamily getInstance() {
            return instance;
        }

        @Override
        public String toString() {
            return "DTT";
        }

    }

    public static class Files extends AdapterFamily {

        public static final AdapterFamily instance = new Files();

        private Files() {}

        public static AdapterFamily getInstance() {
            return instance;
        }

        @Override
        public String toString() {
            return "Files";
        }

    }

    public static class Stream extends AdapterFamily {

        public static final AdapterFamily instance = new Stream();

        private Stream() {}

        public static AdapterFamily getInstance() {
            return instance;
        }

        @Override
        public String toString() {
            return "Stream";
        }

    }

    public static class Sat extends AdapterFamily {

        public static final Map<Satellite, AdapterFamily> instances = new HashMap<Satellite, AdapterFamily>();

        private Satellite satellite;

        private Sat(Satellite satellite) {
            this.satellite = satellite;
        }

        public synchronized static AdapterFamily getInstance(Satellite satellite) {
            if (!instances.containsKey(satellite)) {
                instances.put(satellite, new Sat(satellite));
            }
            AdapterFamily result = instances.get(satellite);
            assert result != null;
            return result;
        }

        public Satellite getSatellite() {
            return satellite;
        }

        @Override
        public String toString() {
            return "Satellite (" + satellite.getName() + ")";
        }

    }

}
