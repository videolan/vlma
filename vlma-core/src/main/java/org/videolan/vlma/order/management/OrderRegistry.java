/*
 * Copyright (C) 2009 the VideoLAN team
 *
 * This file is part of VLMa.
 *
 * VLMa is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * VLMa is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VLMa. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.videolan.vlma.order.management;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.videolan.vlma.model.Order;
import org.videolan.vlma.order.management.OrderState.State;

/**
 * A registry for orders.
 * 
 * This registry associates a state with every order.
 */
public class OrderRegistry {

    private final Map<String, OrderState> orders;

    public OrderRegistry() {
        orders = new HashMap<String, OrderState>();
    }

    public Collection<OrderState> entries() {
        return orders.values();
    }

    public OrderState get(String key) {
        return orders.get(key);
    }

    public void add(Order order) {
        orders.put(order.getId(), new OrderState(order));
    }

    public void update(Order order) {
        String id = order.getId();
        if (!orders.containsKey(id)) {
            throw new IllegalStateException("Updating an order which is not in the registry");
        }
        orders.put(id, new OrderState(order));
    }

    public boolean contains(String order) {
        return orders.containsKey(order);
    }

    public State getState(String order) {
        return orders.get(order).getState();
    }

    public State getState(Order order) {
        return getState(order.getId());
    }

}
