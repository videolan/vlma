/*
 * Copyright (C) 2006-2008 the VideoLAN team
 *
 * This file is part of VLMa.
 *
 * VLMa is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * VLMa is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VLMa. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.videolan.vlma.util;

import java.util.List;

import org.apache.commons.configuration.Configuration;
import org.videolan.vlma.model.AnnouncingStrategy;
import org.videolan.vlma.model.Program;
import org.videolan.vlma.model.StreamingStrategy;

/**
 * A program factory.
 *
 * @author Adrien Grand <jpountz at videolan.org>
 */
public class ProgramFactory {

    private Configuration configuration;

    /**
     * Generates a program.
     *
     * @return the generated program
     */
    @SuppressWarnings("unchecked")
    public Program newProgram() {
        Program p = new Program();
        StreamingStrategy ss = new StreamingStrategy();
        String protocol = configuration.getString("vlma.streaming");
        String encapsulation = configuration.getString("vlma.encapsulation");
        ss.setProtocol(StreamingStrategy.Protocol.valueOf(protocol));
        ss.setEncapsulation(StreamingStrategy.Encapsulation.valueOf(encapsulation));
        p.setStreamingStrategy(ss);
        AnnouncingStrategy as = new AnnouncingStrategy();
        List<String> announcements = configuration.getList("vlma.announcement");
        for (String announcement : announcements) {
            as.addAnnouncement(AnnouncingStrategy.Announcement.valueOf(announcement));
        }
        p.setAnnouncingStrategy(as);
        return p;
    }

    /**
     * @param configuration the configuration to set
     */
    public void setConfiguration(Configuration configuration) {
        this.configuration = configuration;
    }

}
