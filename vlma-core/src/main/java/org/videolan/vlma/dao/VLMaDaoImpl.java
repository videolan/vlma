/*
 * Copyright (C) 2008 the VideoLAN team
 *
 * This file is part of VLMa.
 *
 * VLMa is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * VLMa is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VLMa. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.videolan.vlma.dao;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.apache.commons.configuration.Configuration;
import org.apache.log4j.Logger;
import org.videolan.vlma.exception.NotFoundException;
import org.videolan.vlma.model.Adapter;
import org.videolan.vlma.model.AnnouncingStrategy;
import org.videolan.vlma.model.DVBSAdapter;
import org.videolan.vlma.model.DVBTAdapter;
import org.videolan.vlma.model.FilesAdapter;
import org.videolan.vlma.model.FilesChannel;
import org.videolan.vlma.model.Media;
import org.videolan.vlma.model.Program;
import org.videolan.vlma.model.SatChannel;
import org.videolan.vlma.model.Satellite;
import org.videolan.vlma.model.Server;
import org.videolan.vlma.model.StreamAdapter;
import org.videolan.vlma.model.StreamChannel;
import org.videolan.vlma.model.StreamingStrategy;
import org.videolan.vlma.model.DTTChannel;
import org.videolan.vlma.order.watcher.DirectMulticastStreamWatcher;
import org.videolan.vlma.order.watcher.HttpStreamWatcher;

import com.thoughtworks.xstream.XStream;

public class VLMaDaoImpl implements VLMaDao {

    private static final Logger logger = Logger.getLogger(VLMaDaoImpl.class);
    public static final String DATA_FILE = "data.xml";

    private Configuration configuration;
    private Map<Integer, Media> medias;
    private Map<Integer, Satellite> satellites;
    private Map<Integer, Server> servers;

    private static final Random random = new Random();
    private static final XStream xstream;

    static {
        xstream = new XStream();
        xstream.alias("satellite", Satellite.class);
        xstream.alias("server", Server.class);
        xstream.alias("dvb-t", DVBTAdapter.class);
        xstream.alias("dvb-s", DVBSAdapter.class);
        xstream.alias("filesAdapter", FilesAdapter.class);
        xstream.alias("streamAdapter", StreamAdapter.class);
        xstream.alias("program", Program.class);
        xstream.alias("satChannel", SatChannel.class);
        xstream.alias("dttChannel", DTTChannel.class);
        xstream.alias("filesChannel", FilesChannel.class);
        xstream.alias("streamChannel", StreamChannel.class);
        xstream.alias("httpStreamWatcher", HttpStreamWatcher.class);
        xstream.alias("directMulticastStreamWatcher", DirectMulticastStreamWatcher.class);
        xstream.alias("streamingStrategy", StreamingStrategy.class);
        xstream.alias("announcement", AnnouncingStrategy.Announcement.class);
        xstream.alias("data", DataContainer.class);
        xstream.omitField(Adapter.class, "isUp");
        xstream.omitField(Adapter.class, "isBusy");
        xstream.omitField(Program.class, "adapterName");
        xstream.omitField(Program.class, "broadcastState");
        xstream.omitField(Program.class, "adapter");
        xstream.setMode(XStream.XPATH_ABSOLUTE_REFERENCES);
    }

    public VLMaDaoImpl() {
        medias = new HashMap<Integer, Media>();
        satellites = new HashMap<Integer, Satellite>();
        servers = new HashMap<Integer, Server>();
    }

    public void addAll(Collection<Media> medias) {
        for(Media media : medias) {
            addInternal(media);
        }
        saveToDisk();
    }

    public void add(Media media) {
        addInternal(media);
        saveToDisk();
    }

    private void addInternal(Media media) {
        Integer id;
        synchronized (medias) {
            do {
                id = random.nextInt();
            } while(medias.containsKey(id));
            media.setId(id);
            medias.put(id, media);
        }
    }

    public void add(Satellite satellite) {
        Integer id;
        synchronized (satellites) {
            do {
                id = random.nextInt();
            } while(satellites.containsKey(id));
            satellite.setId(id);
            satellites.put(id, satellite);
        }
        saveToDisk();
    }

    public void add(Server server) {
        Integer id;
        synchronized (satellites) {
            do {
                id = random.nextInt();
            } while(satellites.containsKey(id));
            server.setId(id);
            servers.put(id, server);
        }
        saveToDisk();
    }

    public Media getMedia(int id) {
        Media result;
        synchronized (medias) {
            result = medias.get(id);
        }
        if (result != null) {
            return result;
        }
        throw new NotFoundException("The is no media with ID: " + id + " in the database");
    }

    public List<Media> getMedias() {
        synchronized (medias) {
            List<Media> result = new ArrayList<Media>();
            result.addAll(medias.values());
            return result;
        }
    }

    public Satellite getSatellite(int id) {
        Satellite result;
        synchronized (satellites) {
            result = satellites.get(id);
        }
        if (result != null) {
            return result;
        }
        throw new NotFoundException("The is no satellite with ID: " + id + " in the database");
    }

    public List<Satellite> getSatellites() {
        synchronized (satellites) {
            List<Satellite> result = new ArrayList<Satellite>();
            result.addAll(satellites.values());
            return result;
        }
    }

    public Server getServer(int id) {
        Server result;
        synchronized (servers) {
            result = servers.get(id);
        }
        if (result != null) {
            return result;
        }
        throw new NotFoundException("The is no server with ID: " + id + " in the database");
    }

    public List<Server> getServers() {
        synchronized (servers) {
            List<Server> result = new ArrayList<Server>();
            result.addAll(servers.values());
            return result;
        }
    }

    public void removeAll(Collection<Media> medias) {
        for(Media media : medias) {
            removeInternal(media);
        }
        saveToDisk();
    }

    public void remove(Media media) {
        removeInternal(media);
        saveToDisk();
    }

    private void removeInternal(Media media) {
        int id = media.getId();
        synchronized (medias) {
            if (medias.containsKey(id)) {
                medias.remove(id);
            } else {
                throw new NotFoundException("Media " + media.getName() + " is not in the database.");
            }
        }
    }

    public void remove(Satellite satellite) {
        int id = satellite.getId();
        synchronized (satellites) {
            if (satellites.containsKey(id)) {
                satellites.remove(id);
                synchronized (servers) {
                    for(Server server : servers.values()) {
                        Iterator<Adapter> adapterIt = server.getAdapters().iterator();
                        while(adapterIt.hasNext()) {
                            Adapter adapter = adapterIt.next();
                            if (adapter instanceof DVBSAdapter
                                    && ((DVBSAdapter)adapter).getSatellite().getId() == id) {
                                adapterIt.remove();
                            }
                        }
                    }
                }
                saveToDisk();
            } else {
                throw new NotFoundException("Satellite " + satellite.getName() + " is not in the database.");
            }
        }
    }

    public void remove(Server server) {
        int id = server.getId();
        synchronized (servers) {
            if (servers.containsKey(id)) {
                servers.remove(id);
                synchronized (medias) {
                    for(Media media : medias.values()) {
                        if(media instanceof FilesChannel
                                && ((FilesChannel)media).getServer().getId() == id) {
                            medias.remove(media.getId());
                        }
                    }
                }
                saveToDisk();
            } else {
                throw new NotFoundException("Server " + server.getName() + " is not in the database.");
            }
        }
    }

    public void update(Media media) {
        int id = media.getId();
        synchronized (medias) {
            if (media instanceof FilesChannel && ((FilesChannel) media).getServer() != null) {
                ((FilesChannel) media).setServer(getServer(((FilesChannel) media).getServer().getId()));
            }
            // Should not be necessary, but I've seen some weird cases where
            // the program referenced the wrong media
            for (Program program : media.getPrograms()) {
                program.setMedia(media);
            }
            if (medias.containsKey(id)) {
                medias.put(id, media);
                saveToDisk();
            } else {
                throw new NotFoundException("Media " + media.getName() + " is not in the database.");
            }
        }
    }

    public void update(Satellite satellite) {
        int id = satellite.getId();
        synchronized (satellites) {
            if (satellites.containsKey(id)) {
                satellites.put(id, satellite);
                saveToDisk();
            } else {
                throw new NotFoundException("Satellite " + satellite.getName() + " is not in the database.");
            }
        }
    }

    public void update(Server server) {
        int id = server.getId();
        synchronized (servers) {
            if (servers.containsKey(id)) {
                for (Adapter adapter : server.getAdapters()) {
                    if (adapter instanceof DVBSAdapter) {
                        int satelliteId = ((DVBSAdapter) adapter).getSatellite().getId();
                        ((DVBSAdapter) adapter).setSatellite(getSatellite(satelliteId));
                    }
                }
                servers.put(id, server);
                saveToDisk();
            } else {
                throw new NotFoundException("Server " + server.getName() + " is not in the database.");
            }
        }
    }

    private static class DataContainer {
        protected List<Satellite> satellites;
        protected List<Server> servers;
        protected List<Media> medias;
    }

    synchronized public void saveToDisk(){
        long start = System.currentTimeMillis();
        DataContainer data = new DataContainer();
        data.satellites = getSatellites();
        data.servers = getServers();
        data.medias = getMedias();

        String vlmaData = configuration.getString("vlma.data");

        File dataFile = new File(vlmaData, DATA_FILE);
        FileOutputStream f;
        try {
            f = new FileOutputStream(dataFile);
            xstream.toXML(data, f);
            try {
                f.close();
                logger.debug("Data saved in " + (System.currentTimeMillis() - start) + "ms");
            } catch (IOException e) {
                logger.error("Error while closing " + dataFile, e);
            }
        } catch (FileNotFoundException e) {
            logger.error("Can't save data to " + dataFile, e);
        }
    }

    synchronized public void loadFromDisk() {
        String vlmaData = configuration.getString("vlma.data");
        try {
            File dataFile = new File(vlmaData, DATA_FILE);
            FileInputStream f = new FileInputStream(dataFile);

            DataContainer data;
            data = (DataContainer) xstream.fromXML(f);
            try {
                f.close();
            } catch (IOException e) {
                logger.error("Error closing " + dataFile, e);
            }

            synchronized (satellites) {
                satellites = new HashMap<Integer, Satellite>();
                for (Satellite satellite : (List<Satellite>) data.satellites) {
                    satellites.put(satellite.getId(), satellite);
                }
            }
            synchronized (servers) {
                servers = new HashMap<Integer, Server>();
                for (Server server : (List<Server>) data.servers) {
                    servers.put(server.getId(), server);
                }
                // Default value for boolean is false, but we want isUp
                // to default to true for adapters
                for(Server server : servers.values()) {
                    for(Adapter adapter : server.getAdapters()) {
                        adapter.setUp(true);
                        adapter.setScore(0);
                    }
                }
            }
            synchronized (medias) {
                medias = new HashMap<Integer, Media>();
                for (Media media : (List<Media>) data.medias) {
                    medias.put(media.getId(), media);
                }
            }
        }
        catch (FileNotFoundException e) {
            // This is not really a matter, this file will be created when
            // saveToDisk is called
            logger.warn("Unable to read " + DATA_FILE);
        }
    }

    /**
     * Sets the configuration.
     *
     * @param configuration the configuration to set
     */
    public synchronized void setConfiguration(Configuration configuration) {
        this.configuration = configuration;
    }

}
