/*
 * Copyright (C) 2006-2008 the VideoLAN team
 *
 * This file is part of VLMa.
 *
 * VLMa is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * VLMa is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VLMa. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.videolan.vlma.monitor;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.jrobin.core.Util;
import org.videolan.vlma.dao.VLMaDao;
import org.videolan.vlma.model.Server;
import org.videolan.vlma.notifier.Notifier;
import org.videolan.vlma.server.RrdGraphUpdater;

/**
 * This class is the daemon which monitores servers. It monitores their
 * state using SNMP and creates RRD graphs.
 *
 * @author Sylvain Cadilhac <sylv at videolan.org>
 */
public class ServerMonitor extends Monitor {

    private static final Logger logger = Logger.getLogger(ServerMonitor.class);

    private VLMaDao vlmaDao;
    private RrdGraphUpdater rrdGraphUpdater;

    /**
     * The time interval between two interrogation requests.
     */
    public static final int TIME_INTERVAL = 300;

    public ServerMonitor() {
        registerMonitor(this);
    }

    private Thread serverMonitor;

    private volatile boolean shouldRun;

    public synchronized boolean isRunning() {
        return (serverMonitor != null)
                && (serverMonitor.isAlive());
    }

    /**
     * This method is the main thread which monitores servers. At regular
     * intervals, servers are sent SNMP requests and RRD graphs are updated.
     */
    public void updateRRDGraphs() {
        List<Thread> updateThreads = new ArrayList<Thread>();
        for (Server server : vlmaDao.getServers()) {
            Thread updateThread = new UpdateThread(rrdGraphUpdater, server);
            updateThread.setName("RRD Graph Updater - " + server.getName());
            updateThreads.add(updateThread);
            updateThread.start();
        }
        for (Thread updateThread : updateThreads) {
            try {
                updateThread.join();
            } catch (InterruptedException e) {
                // Wait for the update to finish
            }
        }
    }

    /**
     * This thread is the main thread which starts the VLC checker and the SNMP
     * data updater once before waiting and then looping.
     */
    Runnable serverMonitorDaemon = new Runnable() {
        public void run() {
            shouldRun = true;
            while (shouldRun) {
                try {
                    // Wait before looping
                    Thread.sleep(1000 * (TIME_INTERVAL - (Util.getTime() % TIME_INTERVAL)));
                } catch (InterruptedException e) {
                    continue;
                }
                updateRRDGraphs();
            }
            logger.debug("ServerMonitor thread stopped.");
        }
    };


    public synchronized void start() {
        if (!isRunning()) {
            logger.info("Starting Server Monitor");
            serverMonitor = new Thread(MonitorThreadGroup.getInstance(), serverMonitorDaemon);
            serverMonitor.setName("Server Monitor");
            serverMonitor.start();
        }
    }

    public synchronized void stop() {
        logger.info("Stopping " + this.getClass().getSimpleName());
        shouldRun = false;
        serverMonitor.interrupt();
    }

    /**
     * Sets the VLMa service.
     *
     * @param VLMaDao the VLMaDao to set
     */
    public void setVlmaDao(VLMaDao VLMaDao) {
        this.vlmaDao = VLMaDao;
    }

    /**
     * Sets the graph updater.
     *
     * @param rrdGraphUpdater
     */
    public void setRrdGraphUpdater(RrdGraphUpdater rrdGraphUpdater) {
        this.rrdGraphUpdater = rrdGraphUpdater;
    }

    private static class UpdateThread extends Thread {

        private RrdGraphUpdater graphUpdater;
        private Server server;

        public UpdateThread(RrdGraphUpdater graphUpdater, Server server) {
            this.graphUpdater = graphUpdater;
            this.server = server;
        }

        public void run() {
            logger.debug("Update RRD file of " + server.getName());
            graphUpdater.updateSnmpData(server);
            graphUpdater.updateRrdGraph(server);
        }
    }

    @Override
    protected void onLoadToHigh(Server server, double value) {
        Notifier.dispatchNotification("[WARNING] CPU load of " + server.getName() + " is " + value);
    }

    @Override
    protected void onCpuTooHigh(Server server, double value) {
        Notifier.dispatchNotification("[WARNING] CPU usage of VLC is " + value + "% on " + server.getName());
    }

    @Override
    protected void onMemTooHigh(Server server, double value) {
        Notifier.dispatchNotification("[WARNING] Memory usage of VLC is " + value + "% on " + server.getName());
    }

}
