/*
 * Copyright (C) 2008 the VideoLAN team
 *
 * This file is part of VLMa.
 *
 * VLMa is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * VLMa is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VLMa. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.videolan.vlma.monitor;

import java.io.File;
import java.io.PrintWriter;

import org.apache.log4j.Logger;
import org.videolan.vlma.daemon.Daemon;

/**
 * Common ThreadGroup for monitors.
 * This thread group stops the whole application when an exception is thrown
 * and not caught.
 *
 * @author Adrien Grand
 */
public class MonitorThreadGroup extends ThreadGroup {

    private static final Logger logger = Logger.getLogger(MonitorThreadGroup.class);
    private static final String GROUP_NAME = "Monitor";
    private static final MonitorThreadGroup INSTANCE = new MonitorThreadGroup(GROUP_NAME);
    private static final String CRASH_FILENAME = "vlma_crash";

    public MonitorThreadGroup(String name) {
        super(name);
    }

    public static MonitorThreadGroup getInstance() {
        return INSTANCE;
    }

    @Override
    public void uncaughtException(Thread thread, Throwable t) {
        logger.fatal("Uncaught exception in thread " + thread.getName(), t);
        String filename = CRASH_FILENAME;
        int i = 0;
        do {
            filename = CRASH_FILENAME + "-" + ++i + ".txt";
        } while(new File(filename).exists());
        File f = new File(filename);
        PrintWriter writer = null;
        try {
            if (f.createNewFile() && f.canWrite()) {
                writer = new PrintWriter(f);
                t.printStackTrace(writer);
            }
        } catch (Exception e) {
            logger.error(e);
        } finally {
            if (writer != null) {
                writer.flush();
                writer.close();
            }
        }
        Daemon.getInstance().stop();
    }

}
