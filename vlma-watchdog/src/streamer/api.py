#! /usr/bin/env python
# -*- coding: utf-8 -*-

# Copyright (C) 2009-2010 Adrien Grand
#
# This file is part of VLMa.
#
# VLMa is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# VLMa is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with VLMa. If not, see <http://www.gnu.org/licenses/>.


from constants import *
import logging, math, os, subprocess, sys, threading, time, utils

DEFAULT_STREAMER_OPTIONS = {

  RestartTrigger.CPU_LOAD: 6.0,
  RestartTrigger.STREAMER_CPU: 80.0,
  RestartTrigger.STREAMER_MEMORY: 80.0,

  Logging.DISPLAY_STDOUT_TO: [],
  Logging.DISPLAY_STDERR_TO: [sys.stderr],
  Logging.TAIL_LENGTH: 500,

}

class Streamer:
  """A streamer. Every streamer implementation must inherit this."""

  def __init__(self, sid, cmd, options):
    self._logger = logging.getLogger("watchdog.streamer.%s" %sid)
    self.id = sid
    self.type = "Unknown"
    if utils.which(cmd) is None:
      raise OSError("Cannot find " + cmd + " on the PATH")
    self.cmd = cmd
    self.options = DEFAULT_STREAMER_OPTIONS.copy()
    for k, v in options.items():
      self.options[k] = v
    self.__suspended = False # Used when waiting for the CPU load to decrease
    self.orders = {}

    self.runner = None
    self.__lock = threading.RLock()
    self.__log_queue = []
    self.__log_queue_lock = threading.RLock()
    self.__stdout_reader = LogReader("INFO", self.options[Logging.TAIL_LENGTH], None,
        self.options[Logging.DISPLAY_STDOUT_TO], self.__log_queue, self.__log_queue_lock)
    self.__stderr_reader = LogReader("ERROR", self.options[Logging.TAIL_LENGTH], None,
        self.options[Logging.DISPLAY_STDERR_TO], self.__log_queue, self.__log_queue_lock)
    self.__stdout_reader.start()
    self.__stderr_reader.start()

    self.monitor = StreamerMonitor(self)
    self.monitor.start()

  def _set_suspended(self, suspended):
    self.__lock.acquire()
    self.__suspended = suspended
    self.__lock.release()

  def _get_version(self):
    return "Unknown"

  def _get_log_tail(self):
    self.__log_queue_lock.acquire()
    result = self.__log_queue[:]
    self.__log_queue_lock.release()
    return result

  def _get_uptime(self):
    result = 0.
    self.__lock.acquire()
    if not self.runner is None and self.runner.start_time > 0:
      result = math.floor(time.time() * 1000 - self.runner.start_time)
    self.__lock.release()
    return result

  version = property(lambda s: s._get_version())
  logs = property(lambda s: s._get_log_tail())
  uptime = property(lambda s: s._get_uptime())
  suspended = property(None, _set_suspended)
  cpu = property(lambda s: s.monitor.getStreamerCpu())
  mem = property(lambda s: s.monitor.getStreamerMem())

  def start(self, args):
    self._logger.debug("Runner starting")
    self.__lock.acquire()
    if self.__suspended:
      self._logger.warn("Currently suspended, won't start")
    elif not self.runner is None and self.runner.isAlive():
      self._logger.warn("Cannot start streamer %s, another instance is still running.", self.id)
    else:
      self.runner = StreamerRunner(self, [self.cmd] + args, self.__log_queue, self.__log_queue_lock,
          self.__stdout_reader, self.__stderr_reader)
      self.runner.start()
    self.__lock.release()

  def stop(self):
    self.__lock.acquire()
    if not self.runner is None:
      self._logger.info("Stopping streamer %s", self.id)
      self.runner.stop()
      self.runner = None
    self.__lock.release()

  def restart(self):
    self.__lock.acquire()
    if not self.runner is None:
      self.runner.kill()
    self.__lock.release()

  def can_stream(self, order):
    """Return whether this streamer can stream the provided order"""
    return False

  def start_streaming(self, order):
    """Start streaming the provided order"""
    pass

  def stop_streaming(self, order):
    """Stop streaming the provided order"""
    pass


class StreamerRunner(threading.Thread):
  """A streamer runner"""

  def __init__(self, streamer, args, queue, queue_lock, stdout_reader, stderr_reader):
    self.__streamer = streamer
    self.__args = args
    self.__logger = logging.getLogger("watchdog.streamer.%s.runner" %self.__streamer.id)
    threading.Thread.__init__(self)
    self.setName("Streamer Runner: " + self.__streamer.id)
    self.__lock = threading.RLock()
    self.pid = 0
    self.__shouldRun = True
    self.start_time = 0
    self.__last_started = 0
    self.__log_queue = queue
    self.__log_queue_lock = queue_lock
    self.__stdout_reader = stdout_reader
    self.__stderr_reader = stderr_reader

  def run(self):
    while True:
      self.__lock.acquire()
      if not self.__shouldRun:
        self.__lock.release()
        break
      if self.__last_started > 1000L * (time.time() - 5):
        to_sleep = int(5 + self.__last_started / 1000 - time.time())
        self.__logger.info("Waiting %d seconds before restarting...", to_sleep)
        time.sleep(to_sleep)
      self.start_time = math.floor(1000L * time.time())
      self.__logger.info("Running %s", " ".join(self.__args))
      self.__last_started = self.start_time
      try:
        process = subprocess.Popen(self.__args, bufsize=8192, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        self.__stdout_reader.set_input(process.stdout)
        self.__stderr_reader.set_input(process.stderr)
        self.pid = process.pid
        self.__logger.info("%s: PID is %d", self.__streamer.id, self.pid)
      except BaseException, e:
        self.__logger.warn("Error, cannot start streamer %d: %s", self.pid, str(e))
        break
      except:
        self.__logger.warn("Error, cannot start streamer %d", self.pid)
        break
      finally:
        self.__lock.release()
      exit_status = process.wait()
      self.__lock.acquire()
      self.pid = None
      self.start_time = 0
      self.__lock.release()
      self.__logger.info("Streamer %s exited with return code %d", self.__streamer.id, exit_status)

  def kill(self):
    pid = self.pid
    if pid > 0:
      try:
        if utils.platform_is_windows:
          handle = win32api.OpenProcess(1, False, pid)
          win32api.TerminateProcess(handle, 0)
          win32api.CloseHandle(handle)
        else:
          os.kill(pid, 9)
      except Exception, e:
        #  Process already stopped
        self.__logger.info(e)
    self.start_time = 0

  def stop(self):
    self.__lock.acquire()
    self.__shouldRun = False
    self.__lock.release()
    self.kill()
    self.join()


class LogReader(threading.Thread):
  """A thread which consumes the running program logs in order to make them
  available from the Web interface"""

  def __init__(self, sid, size, input, files, output_queue, output_lock):
    threading.Thread.__init__(self)
    self.__id = sid
    self.__size = size
    self.__lock = threading.RLock()
    self.__output_lock = output_lock
    self.__files = files
    self.__output = output_queue
    self.__input = input
    self.setDaemon(True)

  def set_input(self, input):
    self.__lock.acquire()
    self.__input = input
    self.__lock.release()

  def run(self):
    while True:
      self.__lock.acquire()
      line = ""
      if not self.__input is None:
        line = self.__input.readline().rstrip('\n')
      self.__lock.release()
      if line == "":
        time.sleep(1)
        continue
      self.__output_lock.acquire()
      self.__output.append((self.__id, line))
      for file in self.__files:
        file.write(line)
        file.write('\n')
      to_remove = len(self.__output) - self.__size
      if to_remove > 0:
        for i in range(to_remove):
          self.__output.pop(0)
      self.__output_lock.release()


class StreamerMonitor(threading.Thread):
  """A streamer monitor. Restarts the streamer whenever required."""

  def __init__(self, streamer):
    self.__logger = logging.getLogger("watchdog.streamer.%s.monitor" %streamer.id)
    threading.Thread.__init__(self)
    self.__streamer = streamer
    self.setDaemon(True)

  def run(self):
    while(True):
      time.sleep(1)
      cpuLoad = utils.getCpuLoad()
      if cpuLoad >= self.__streamer.options[RestartTrigger.CPU_LOAD]:
        logger.warn("CPU load is %f, Streamer restart triggered for %s", cpuLoad, self.__streamer.id)
        # Prevent someone from restarting VLC while the load is too high
        self.__streamer.suspended = true
        self.__streamer.stop()
        # Because the load won't go down in one millisecond
        self.__logger.info("Waiting for the load to decrease")
        while True:
          time.sleep(5)
          if utils.getCpuLoad() < self.__streamer.options[RestartTrigger.CPU_LOAD]:
            break
        self.__streamer.suspended(False)
        self.__streamer.start()
        continue
      streamerCpu = self.getStreamerCpu()
      if streamerCpu >= self.__streamer.options[RestartTrigger.STREAMER_CPU]:
        logger.warn("CPU usage of Streamer %s is %f, restart triggered", self.__streamer.id, streamerCpu)
        self.__streamer.restart()
        continue
      streamerMem = self.getStreamerMem()
      if streamerMem >= self.__streamer.options[RestartTrigger.STREAMER_MEMORY]:
        logger.warn("Memory usage of Streamer %s is %f, restart triggered", self.__streamer.id, streamerMem)
        self.__streamer.restart()
        continue

  def _getPidProperty(self, property):
    runner = self.__streamer.runner
    if not runner is None and runner.pid > 0:
      try:
        cmd = "ps -p %d -o %s=" %(runner.pid, property)
        result = os.popen(cmd).readline().strip(' ').strip('\n')
        if result == "":
          return 0
        else:
          return float(result)
      except Exception, e:
        self.__logger.warn(e)
    return 0.

  def getStreamerCpu(self):
    if utils.platform_is_windows:
      # TODO
      return 0.
    else:
      return self._getPidProperty("pcpu")

  def getStreamerMem(self):
    if utils.platform_is_windows:
      # TODO
      return 0.
    else:
      return self._getPidProperty("pmem")


class Order:
  """Base class for streaming orders"""

  def __init__(self, id):
    self.id = id
    # In case of a DVB order, src is a SID. It is the URI of the resource to
    # stream otherwise.
    self.programs = {} # src -> dest[]
    self.ttl = 12
    self.streamer = None


class Dest:
  """A destination. Describes what to do with the stream (how and where to
  stream, transcoding, etc.)"""

  def __init__(self, order, ip="0.0.0.0", port=1234):
    self.__order = order
    self.ip = ip
    self.port = port
    self.streaming   = Streaming()
    self.transcoding = None
    self.announcing  = None

  ttl = property(lambda s: s.__order.ttl)


class Streaming:
  """Describes how to stream."""

  def __init__(self, type="broadcast", protocol="udp", mux="raw"):
    self.type = type
    self.protocol = protocol
    self.mux = mux
    self.loop = False # Only used for files


class Transcoding:
  """Describes how to transcode the stream."""

  def __init__(self):
    self.vcodec = "mp2v"
    self.acodec = "mpga"
    self.vb = 800
    self.scale = 1.
    self.ab = 128
    self.deinterlace = False


class Announcing:
  """Describes how the stream should be announced"""

  def __init__(self, type="sap", name="", group=None, description=None):
    self.type = type
    self.name = name
    self.group = group
    self.description = None


class DVBOrder(Order):
  """Base class for DVB orders"""

  def __init__(self, id):
    Order.__init__(self, id)
    self.adapter = 0
    self.frequency = 0


class DVBTOrder(DVBOrder):
  """A DVB-T order"""

  def __init__(self, id):
    DVBOrder.__init__(self, id)


class DVBSOrder(DVBOrder):
  """A DVB-S order"""

  def __init__(self, id):
    DVBOrder.__init__(self, id)
    self.fec = 9        # the error correction
    self.voltage = 13   # 13 for vertical polarization, 18 for horizontal
    self.srate = 0      # symbol rate
    self.bandwidth = 8  # DVB bandwidth


class StreamOrder(Order):
  """A stream order"""

  def __init__(self, id):
    Order.__init__(self, id)


class FilesOrder(Order):
  """A files order"""

  def __init__(self, id):
    Order.__init__(self, id)


