/*
 * Copyright (C) 2006-2008 the VideoLAN team
 *
 * This file is part of VLMa.
 *
 * VLMa is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * VLMa is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VLMa. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.videolan.vlma.web.controller.media.satellite;

import java.net.MalformedURLException;
import java.net.URL;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

public class SatChannelUpdateValidator implements Validator {

    @SuppressWarnings("unchecked")
    public boolean supports(Class arg0) {
        return arg0.equals(SatChannelUpdate.class);
    }

    public void validate(Object arg0, Errors arg1) {
        SatChannelUpdate mediasSatUpdate = (SatChannelUpdate) arg0;

        if (mediasSatUpdate == null) {
            arg1.rejectValue("url", "medias.sat.update.error.not-specified");
            return;
        } else {
            try {
                new URL(mediasSatUpdate.getUrl());
            } catch (MalformedURLException e) {
                arg1.rejectValue("url", "medias.sat.update.error.invalidurl");
                return;
            }
        }
    }

}
