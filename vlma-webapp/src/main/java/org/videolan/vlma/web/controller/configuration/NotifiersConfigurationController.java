/*
 * Copyright (C) 2008 the VideoLAN team
 *
 * This file is part of VLMa.
 *
 * VLMa is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * VLMa is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VLMa. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.videolan.vlma.web.controller.configuration;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.view.RedirectView;
import org.videolan.vlma.Data;

public class NotifiersConfigurationController extends SimpleFormController {

    private Data data;

    public void setData(Data data) {
        this.data = data;
    }

    @Override
    public ModelAndView onSubmit(Object command) throws Exception {
        NotifiersConfiguration form = (NotifiersConfiguration) command;
        data.setProperty("vlma.notification.mail.host", form.getVlma_notification_mail_host());
        data.setProperty("vlma.notification.mail.sender", form.getVlma_notification_mail_sender());
        String mailRecipients = form.getVlma_notification_mail_recipients();
        if (!"".equals(mailRecipients.trim()))
            data.setProperty("vlma.notification.mail.recipients", mailRecipients.split(","));
        else
            data.clearProperty("vlma.notification.mail.recipients");
        data.setProperty("vlma.notification.irc.host", form.getVlma_notification_irc_host());
        data.setProperty("vlma.notification.irc.port", form.getVlma_notification_irc_port().toString());
        data.setProperty("vlma.notification.irc.nick", form.getVlma_notification_irc_nick());
        data.setProperty("vlma.notification.irc.pass", form.getVlma_notification_irc_pass());
        String chans = form.getVlma_notification_irc_chan();
        if (!"".equals(chans.trim()))
            data.setProperty("vlma.notification.irc.chan", chans.split(","));
        else
            data.clearProperty("vlma.notification.irc.chan");
        data.setProperty("vlma.notification.msn.login", form.getVlma_notification_msn_login());
        data.setProperty("vlma.notification.msn.pass", form.getVlma_notification_msn_pass());
        String msnRecipients =  form.getVlma_notification_msn_recipients();
        if (!"".equals(msnRecipients.trim()))
            data.setProperty("vlma.notification.msn.recipients", msnRecipients.split(","));
        else
            data.clearProperty("vlma.notification.msn.recipients");
        return new ModelAndView(new RedirectView(getSuccessView()));
    }

    private static String listToString(List<?> list) {
        StringBuilder result = new StringBuilder();
        for(Object o : list) {
            if(!"".equals(o.toString()))
                result.append(",").append(o.toString());
        }
        return result.length() > 0 ? result.substring(1) : "";
    }

    @Override
    protected Object formBackingObject(HttpServletRequest request) throws Exception {
        NotifiersConfiguration form = new NotifiersConfiguration();
        form.setVlma_notification_mail_host(data.getString("vlma.notification.mail.host"));
        form.setVlma_notification_mail_sender(data.getString("vlma.notification.mail.sender"));
        form.setVlma_notification_mail_recipients(listToString(data.getList("vlma.notification.mail.recipients")));
        form.setVlma_notification_irc_host(data.getString("vlma.notification.irc.host"));
        form.setVlma_notification_irc_port(data.getInt("vlma.notification.irc.port"));
        form.setVlma_notification_irc_nick(data.getString("vlma.notification.irc.nick"));
        form.setVlma_notification_irc_pass(data.getString("vlma.notification.irc.pass"));
        form.setVlma_notification_irc_chan(listToString(data.getList("vlma.notification.irc.chan")));
        form.setVlma_notification_msn_login(data.getString("vlma.notification.msn.login"));
        form.setVlma_notification_msn_pass(data.getString("vlma.notification.msn.pass"));
        form.setVlma_notification_msn_recipients(listToString(data.getList("vlma.notification.msn.recipients")));
        return form;
    }

}

