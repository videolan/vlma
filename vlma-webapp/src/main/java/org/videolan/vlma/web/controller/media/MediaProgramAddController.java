/*
 * Copyright (C) 2006-2008 the VideoLAN team
 *
 * This file is part of VLMa.
 *
 * VLMa is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * VLMa is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VLMa. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.videolan.vlma.web.controller.media;

import java.io.IOException;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.view.RedirectView;
import org.videolan.vlma.Data;
import org.videolan.vlma.model.DTTChannel;
import org.videolan.vlma.model.FilesChannel;
import org.videolan.vlma.model.Media;
import org.videolan.vlma.model.Program;
import org.videolan.vlma.model.SatChannel;
import org.videolan.vlma.model.StreamChannel;
import org.videolan.vlma.model.TranscodingStrategy;
import org.videolan.vlma.model.StreamingStrategy.Type;

public class MediaProgramAddController extends SimpleFormController {

    private Data data;

    private String successViewDTT;

    private String successViewSat;

    private String successViewFile;

    private String successViewStream;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public String getSuccessViewSat() {
        return successViewSat;
    }

    public String getSuccessViewDTT() {
        return successViewDTT;
    }

    public void setSuccessViewDTT(String successViewDTT) {
        this.successViewDTT = successViewDTT;
    }

    public void setSuccessViewSat(String successViewSat) {
        this.successViewSat = successViewSat;
    }

    public String getSuccessViewFile() {
        return successViewFile;
    }

    public void setSuccessViewFile(String successViewFile) {
        this.successViewFile = successViewFile;
    }

    public String getSuccessViewStream() {
        return successViewStream;
    }

    public void setSuccessViewStream(String successViewStream) {
        this.successViewStream = successViewStream;
    }

    @Override
    public ModelAndView onSubmit(Object command) throws ServletException,
            MalformedURLException, IOException {
        MediaProgramAdd mediasProgramAdd = (MediaProgramAdd) command;

        int mediaId = mediasProgramAdd.getMediaId();
        Media media = data.getMedia(mediaId);

        Program program = data.newProgram();
        program.setMedia(media);
        program.setId(mediasProgramAdd.getId());
        program.setPriority(Integer.parseInt(mediasProgramAdd.getPriority()));
        program.setName(mediasProgramAdd.getSap());
        program.setGroup(mediasProgramAdd.getGroup());
        program.getStreamingStrategy().setType(mediasProgramAdd.getType());
        program.getStreamingStrategy().setProtocol(mediasProgramAdd.getProtocol());
        program.getStreamingStrategy().setEncapsulation(mediasProgramAdd.getMux());
        program.getAnnouncingStrategy().setAnnouncements(mediasProgramAdd.getAnnouncements());
        String ip = mediasProgramAdd.getIp();
        if (ip != null && ip.length() > 0) {
            program.setIp(InetAddress.getByName(ip));
        }
        if (mediasProgramAdd.getTranscodeAudio() || mediasProgramAdd.getTranscodeVideo()) {
            TranscodingStrategy ts = new TranscodingStrategy();
            if (mediasProgramAdd.getTranscodeAudio()) {
                ts.setAudioCodec(mediasProgramAdd.getAudioCodec());
                ts.setAudioBitrate(mediasProgramAdd.getAudioBitrate());
            } else {
                ts.setAudioCodec(null);
            }
            if (mediasProgramAdd.getTranscodeVideo()) {
                ts.setVideoCodec(mediasProgramAdd.getVideoCodec());
                ts.setVideoBitrate(mediasProgramAdd.getVideoBitrate());
                ts.setDeinterlace(mediasProgramAdd.getDeinterlace());
                ts.setScale(mediasProgramAdd.getScale());
            } else {
                ts.setVideoCodec(null);
            }
            program.setTranscodingStrategy(ts);
        } else {
            program.setTranscodingStrategy(null);
        }
        media.addProgram(program);
        data.update(media);

        // Determine the success view with the media class
        String successView = null;
        if (media instanceof FilesChannel) {
            successView = getSuccessViewFile();
        }
        else if (media instanceof StreamChannel) {
            successView = getSuccessViewStream();
        }
        else if (media instanceof SatChannel) {
            successView = getSuccessViewSat();
        } else {
            successView = getSuccessViewDTT();
        }

        return new ModelAndView(new RedirectView(successView));
    }

    @Override
    protected Object formBackingObject(HttpServletRequest request) throws Exception {
        MediaProgramAdd mediasProgramAdd = new MediaProgramAdd();
        int mediaId = Integer.parseInt(request.getParameter("media"));
        Media media = data.getMedia(mediaId);

        /* Remove the VoD streaming strategy if the media is not a file channel */
        Type[] types = Type.values();
        Type[] availableTypes;
        if(media instanceof FilesChannel) {
            availableTypes = types;
        } else {
            availableTypes = new Type[types.length - 1];
            int i = 0;
            for(Type type : types) {
                if(type != Type.VOD)
                    availableTypes[i++] = type;
            }
        }
        mediasProgramAdd.setTypes(availableTypes);

        mediasProgramAdd.setPriority("10");
        if (media instanceof SatChannel){
            String country = ((SatChannel)media).getCountry();
            if (country != null && country.length() > 0) {
                mediasProgramAdd.setSap("[" + country + "] " + media.getName());
            } else {
                mediasProgramAdd.setSap(media.getName());
            }
        }
        mediasProgramAdd.setSap(media.getName());
        String sapGroupId = null;
        if (media instanceof FilesChannel) {
            sapGroupId = "vlma.announcement.sap.files.group";
        } else if (media instanceof StreamChannel) {
            sapGroupId = "vlma.announcement.sap.stream.group";
        } else if (media instanceof DTTChannel) {
            sapGroupId = "vlma.announcement.sap.dtt.group";
        } else if (media instanceof SatChannel) {
            String category = ((SatChannel) media).getCategory();
            if ("R-DIG".equals(category) || "R-DIG-CRYPT".equals(category)) {
                sapGroupId = "vlma.announcement.sap.radio.group";
            } else {
                sapGroupId = "vlma.announcement.sap.satellite.group";
            }
        }
        if (sapGroupId != null)
            mediasProgramAdd.setGroup(data.getString(sapGroupId));
        else
            mediasProgramAdd.setGroup("");
        mediasProgramAdd.setIp("");
        mediasProgramAdd.setMediaId(mediaId);
        mediasProgramAdd.setId(slugify(media.getName()));
        Program program = data.newProgram();
        mediasProgramAdd.setType(program.getStreamingStrategy().getType());
        mediasProgramAdd.setProtocol(program.getStreamingStrategy().getProtocol());
        mediasProgramAdd.setMux(program.getStreamingStrategy().getEncapsulation());
        mediasProgramAdd.setAnnouncements(program.getAnnouncingStrategy().getAnnouncements());
        mediasProgramAdd.setScale(1d);
        mediasProgramAdd.setVideoBitrate(800);
        mediasProgramAdd.setAudioBitrate(128);
        return mediasProgramAdd;
    }

    @SuppressWarnings("unchecked")
    @Override
    protected ModelAndView showForm(HttpServletRequest arg0,
            HttpServletResponse arg1, BindException arg2, Map arg3)
            throws Exception {
        int mediaId = Integer.parseInt(arg0.getParameter("media"));
        Map<String, Object> m = new HashMap<String, Object>();
        m.put("media", data.getMedia(mediaId));
        return super.showForm(arg0, arg1, arg2, m);
    }

    private static String slugify(String text) {
        return text.replaceAll("[^a-zA-Z0-9]", "");
    }

}
