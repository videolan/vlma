/*
 * Copyright (C) 2006-2008 the VideoLAN team
 *
 * This file is part of VLMa.
 *
 * VLMa is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * VLMa is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VLMa. If not, see <http://www.gnu.org/licenses/>.
 *
 */

package org.videolan.vlma.web.controller.media;

import java.rmi.RemoteException;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.videolan.vlma.Data;

public class MediaProgramRemoveValidator implements Validator {

    private Data data;

    @SuppressWarnings("unchecked")
    public boolean supports(Class arg0) {
        return arg0.equals(MediaProgramRemove.class);
    }

    public void validate(Object arg0, Errors arg1) {
        MediaProgramRemove mediasProgramRemove = (MediaProgramRemove) arg0;

        if (mediasProgramRemove == null) {
            arg1.rejectValue("sap",
                            "medias.program.remove.error.not-specified");
            return;
        } else {
            try {
                if (data.getMedia(
                        mediasProgramRemove.getMediaId()) == null) {
                    arg1.rejectValue("sap",
                            "medias.program.remove.error.nonexisting");
                    return;
                }
            } catch (RemoteException e) {
                arg1.rejectValue("sap", "error.remote_exception");
            }
        }

    }

    /**
     * @param data the data to set
     */
    public void setData(Data data) {
        this.data = data;
    }

}
