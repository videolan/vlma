<%@ include file="/WEB-INF/jsp/include.jsp" %>

<ul>
    <li><a href="ordermonitor.htm"><fmt:message key="orders.monitor" /></a></li>
    <li><a href="orderstartmonitoring.htm"><fmt:message key="orders.startmonitoring" /></a></li>
    <li><a href="orderinteractive.htm"><fmt:message key="orders.interactive" /></a></li>
</ul>
